local map = vim.keymap.set
local opts = { noremap = true, silent = true }


-- Map Explore function
map('n', '<leader>;','<cmd>Explore<CR>', opts)

-- Rubocop 
map('n', '<leader>r', '<cmd>!rubocop -a<CR>', opts)
-- Enables line numbers
vim.o.number = true
vim.o.relativenumber = true

-- Enables termguicolors
vim.o.termguicolors = true

-- Enables sytem clipboard
vim.o.clipboard = "unnamedplus"

-- Enables splits
vim.o.splitright = true
vim.o.splitbelow = true

vim.opt.tabstop = 2
vim.opt.softtabstop = 2
vim.opt.shiftwidth = 2
vim.opt.expandtab = true

