return {
  {
    'akinsho/toggleterm.nvim',
    version = "*",
    lazy = false,
    opts = { --[[ things you want to change go here]] },
    config = function()
      require("toggleterm").setup {
        size = 80,
        direction = 'vertical',
        open_mapping = [[<c-n>]],
        hide_numbers = true,
        terminal_mappings = true,
      }
      function _G.set_terminal_keymaps()
        local opts = { buffer = 0 }
        vim.keymap.set('t', '<esc>', [[<C-\><C-n>]], opts)
        vim.keymap.set('t', 'jk', [[<C-\><C-n>]], opts)
        vim.keymap.set('t', '<C-h>', [[<Cmd>wincmd h<CR>]], opts)
        vim.keymap.set('t', '<C-j>', [[<Cmd>wincmd j<CR>]], opts)
        vim.keymap.set('t', '<C-l>', [[<Cmd>wincmd l<CR>]], opts)
      end

      vim.cmd('autocmd! TermOpen term://* lua set_terminal_keymaps()')
    end
  }
}
