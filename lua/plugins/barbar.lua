return {
  {
    'romgrk/barbar.nvim',
    lazy = false,
    dependencies = {
      'lewis6991/gitsigns.nvim',     -- OPTIONAL: for git status
      'nvim-tree/nvim-web-devicons', -- OPTIONAL: for file icons
    },
    keys = {
      {"<leader>1", "<cmd>BufferGoto 1<cr>", desc = "Go to buffer 1" },
      {"<leader>2", "<cmd>BufferGoto 2<cr>", desc = "Go to buffer 2" },
      {"<leader>3", "<cmd>BufferGoto 3<cr>", desc = "Go to buffer 3" },
      {"<leader>4", "<cmd>BufferGoto 4<cr>", desc = "Go to buffer 4" },
      {"<leader>5", "<cmd>BufferGoto 5<cr>", desc = "Go to buffer 5" },
      {"<leader>6", "<cmd>BufferGoto 6<cr>", desc = "Go to buffer 6" },
      {"<leader>7", "<cmd>BufferGoto 7<cr>", desc = "Go to buffer 7" },
      {"<leader>8", "<cmd>BufferGoto 8<cr>", desc = "Go to buffer 8" },
      {"<leader>9", "<cmd>BufferGoto 9<cr>", desc = "Go to buffer 9" },
      {"<leader>0", "<cmd>BufferLast<cr>", desc = "Go to last buffer" },
      {"<leader>q", "<cmd>BufferClose<cr>", desc = "Close buffer" },
      {"<leader>h", "<cmd>BufferPrevious<cr>", desc = "Previous buffer" },
      {"<leader>l", "<cmd>BufferNext<cr>", desc = "Next buffer" },
      {"<leader>bd", "<cmd>BufferCloseAllButCurrent<cr>", desc = "Close all but current buffer" },
      {"<leader>bo", "<cmd>BufferCloseBuffersLeft<cr>", desc = "Close all buffers to the left" },
      {"<leader>bc", "<cmd>BufferCloseBuffersRight<cr>", desc = "Close all buffers to the right" },
      {"<leader>bs", "<cmd>BufferPick<cr>", desc = "Pick buffer" },
      {"<leader>be", "<cmd>BufferCloseAllButPinned<cr>", desc = "Close all but pinned buffers" },
      {"<leader>bp", "<cmd>BufferPin<cr>", desc = "Pin buffer" },
      {"<leader>bu", "<cmd>BufferUnpin<cr>", desc = "Unpin buffer" },
    },
    init = function() vim.g.barbar_auto_setup = false end,
    opts = {
      -- lazy.nvim will automatically call setup for you. put your options here, anything missing will use the default:
      -- animation = true,
      -- insert_at_start = true,
      -- …etc.
    },
    version = '^1.0.0', -- optional: only update when a new 1.x version is released
  },
}
