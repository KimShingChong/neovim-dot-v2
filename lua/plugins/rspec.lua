return {
  'thoughtbot/vim-rspec',
  cmd = { 'Rspec' },
  keys = {
    { "<leader>a", "<cmd>call RunAllSpecs()<cr>", desc = "Run all specs" },
    { "<leader>t", "<cmd>call RunCurrentSpecFile()<cr>", desc = "Run pecs" },
  }
}
